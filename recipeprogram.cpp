#include <iostream>
using namespace std;
 int main()
{
 float cupsOfButter = 1;
 float cupsOfSugar = 1;
 float eggs = 2;
 float tsVanilla = 2;
 float tsBakingSoda = 1;
 float tsSalt = 0.5;
 float cupsFlour = 3;
 float cupsChocolate = 2;
    float batchSize;
    
    cout<< “Ingredient List” << endl;
    cout<< cupsOfButter << " cups of butter " << endl;
    cout<< cupsOfSugar  << " cups of sugar " << endl;
    cout<< eggs << " eggs " << endl;
    cout<< tsVanilla << " teaspoons of Vanilla " << endl;
    cout<< tsBakingSoda << " teaspoons of Baking Soda " << endl; # how many tablespoons you add
    cout<< tsSalt << " teaspoons of salt " << endl;
    cout<< cupsFlour << “ cups of Flour ” << endl;
    cout<< cupsChocolate << “ cups of Chocolate chips ” << endl;
    
    cout<< " how many batch size: "
    cin>> batchSize;
    
    cupsOfButter = cupsOfButter * batchSize;
    cupsOfSugar = cupsOfSugar * batchSize;
    eggs = eggs * batchSize;
    tsVanilla = tsVanilla * batchSize;
    tsBakingSoda = tsBakingSoda * batchSize;
    tsSalt = tsSalt * batchSize;
    cupsFlour = cupsFlour * batchSize;
    cupsChocolate = cupsChocolate * batchSize;
   
    cout<< “Adjusted Ingredient List” << endl;
    cout<< cupsOfButter << " cups of butter " << endl;
    cout<< cupsOfSugar  << " cups of sugar " << endl;
    cout<< eggs << " eggs " << endl;
    cout<< tsVanilla << " teaspoons of Vanilla " << endl;
    cout<< tsBakingSoda << " teaspoons of Baking Soda " << endl;
    cout<< tsSalt << " teaspoons of salt " << endl;
    cout<< cupsFlour << “ cups of Flour ” << endl;
    cout<< cupsChocolate << “ cups of Chocolate chips ” << endl;
    
    

return 0;
}

